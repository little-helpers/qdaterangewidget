from qtpy.QtCore import QDate
from typing import Generator


def date_range(from_date: QDate, to_date: QDate) -> Generator:
    date = from_date
    while date <= to_date:
        yield date
        date = date.addDays(1)


