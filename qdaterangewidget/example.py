import sys
from qtpy.QtWidgets import QWidget, QVBoxLayout, QPushButton, QMessageBox, QApplication

from qdaterangewidget import QDateRangeWidget, date_range


class MyApp(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Retrieve Date Range")

        self.layout = QVBoxLayout()
        self.setLayout(self.layout)

        self.calendar = QDateRangeWidget()
        self.layout.addWidget(self.calendar)

        btn = QPushButton("Get Date Range", clicked=self.print_days_selected)
        self.layout.addWidget(btn)

    def print_days_selected(self):
        if self.calendar.from_date and self.calendar.to_date:
            date_list = [date.toString() for date in date_range(self.calendar.from_date, self.calendar.to_date)]

            msg_box = QMessageBox()
            msg_box.setText("\n".join(date_list))
            msg_box.exec_()


if __name__ == '__main__':
    app = QApplication(sys.argv)

    my_app = MyApp()
    my_app.show()

    sys.exit(app.exec_())
