from typing import Optional

from qtpy.QtGui import QTextCharFormat, QPalette
from qtpy.QtWidgets import QCalendarWidget
from qtpy.QtCore import QDate

from qdaterangewidget import date_range


class QDateRangeWidget(QCalendarWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.from_date: Optional[QDate] = self.selectedDate()
        self.to_date: Optional[QDate] = None

        # set highlighting of the selected date range
        self.highlight_format = QTextCharFormat()
        self.highlight_format.setBackground(self.palette().brush(QPalette.Highlight))
        self.highlight_format.setForeground(self.palette().color(QPalette.HighlightedText))

        self.clicked.connect(self.date_is_clicked)

    def date_is_clicked(self, date):
        # clear highlighting of previously selected date range
        self.format_range(QTextCharFormat())

        if not self.from_date and not self.to_date:
            # If no date was selected click means from_date
            self.from_date = date
        elif not self.to_date:
            # if from_date was selected and to_date not yet, set to_date. But make sure that from_date is before to_date
            if date >= self.from_date:
                self.to_date = date
            else:
                self.to_date = self.from_date
                self.from_date = date
        else:
            # from and to dates were already selected then reset the selection
            self.from_date = date
            self.to_date = None

        self.format_range(self.highlight_format)

    def format_range(self, date_format):
        if self.from_date and self.to_date:
            for date in date_range(self.from_date, self.to_date):
                self.setDateTextFormat(date, date_format)
