import unittest

from qtpy.QtCore import QDate

from qdaterangewidget import date_range


class TestDateRange(unittest.TestCase):
    def test_empty_to(self):
        self.assertListEqual(list(date_range(QDate.currentDate(), None)), [])

    def test_date_range(self):
        date_range_ = date_range(QDate(2023, 7, 17), QDate(2023, 7, 20))
        self.assertListEqual(list(date_range_),
                             [QDate(2023, 7, 17), QDate(2023, 7, 18), QDate(2023, 7, 19), QDate(2023, 7, 20)])

    def test_date_range_equal_from_to(self):
        self.assertListEqual(list(date_range(QDate.currentDate(), QDate.currentDate())), [QDate.currentDate()])

    def test_date_range_to_before_from(self):
        self.assertListEqual(list(date_range(QDate(2023, 7, 17), QDate(2023,7, 16))), [])


if __name__ == '__main__':
    unittest.main()
