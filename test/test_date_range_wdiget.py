import unittest

from qtpy.QtWidgets import QApplication
from qtpy.QtCore import QDate

from qdaterangewidget import QDateRangeWidget, date_range

app = QApplication()


class DateRangeWidgetTest(unittest.TestCase):
    def setUp(self) -> None:
        self.daterange = QDateRangeWidget()

    def test_init(self):
        self.assertEqual(self.daterange.from_date, QDate.currentDate())
        self.assertEqual(self.daterange.to_date, None)

    def test_add_date(self):
        add_date = QDate.currentDate().addDays(3)
        self.daterange.date_is_clicked(add_date)

        self.assertEqual(self.daterange.from_date, QDate.currentDate())
        self.assertEqual(self.daterange.to_date, add_date)

    def test_add_before_date(self):
        add_date = QDate.currentDate().addDays(-3)
        self.daterange.date_is_clicked(add_date)

        self.assertEqual(self.daterange.from_date, add_date)
        self.assertEqual(self.daterange.to_date, QDate.currentDate())

    def test_same_date(self):
        self.daterange.date_is_clicked(QDate.currentDate())
        self.assertEqual(self.daterange.to_date, QDate.currentDate())
        self.assertEqual(self.daterange.from_date, QDate.currentDate())

    def test_add_two_dates(self):
        date_1 = QDate.currentDate().addDays(1)
        date_2 = QDate.currentDate().addDays(2)
        self.daterange.date_is_clicked(date_1)
        self.daterange.date_is_clicked(date_2)

        self.assertEqual(self.daterange.from_date, date_2)
        self.assertEqual(self.daterange.to_date, None)

    def test_add_three_dates(self):
        date_list = [QDate.currentDate().addDays(days) for days in range(3)]

        for date in date_list:
            self.daterange.date_is_clicked(date)

        self.assertEqual(self.daterange.from_date, date_list[-2])
        self.assertEqual(self.daterange.to_date, date_list[-1])

    def test_format(self):
        self.daterange.date_is_clicked(QDate.currentDate().addDays(4))
        truth = {date: self.daterange.highlight_format for date in date_range(self.daterange.from_date, self.daterange.to_date)}
        self.assertDictEqual(truth, self.daterange.dateTextFormat())


if __name__ == '__main__':
    unittest.main()
